from django.urls import path
from todos.views import todo_list_list, todo_list_detail, create_list, todo_list_update, todo_list_delete

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),\
    path("<int:id>/",todo_list_detail, name="todo_list_detail"),
    path("create/", create_list, name="todo_list_create"),
    path("<int:id>/edit/", todo_list_update, name="todo_list_update"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete")
]
